# root

Contains root elements: 
- Design templates
- Docker container for CI/CD

Changes in this repo has an affect of all other repos. 

## Setup

### docker
Contains the dockerfile to build *docker image*.\
The *docker image* is responsible to build the hugo webpage in the ci/cd pipeline. 

### hugo
Contains the style files for the hugo pages. The hugo pages refer to this module. 

## How-to

### Pre-Requirements

- Verified GitLab Account (Credit card; free from costs)
- Installation von gohugo **or** Docker

### New project (blank)

1. Create new repository
    - Name of the repository will be used as URL
    - Name of the repositories and description will be used on webpage

1. Copy files from an existing repository (template)

1. Clear files in folder `/content`

### Manage

#### Chapter

`hugo new --kind chapter intro/_index.md`

#### Page

`hugo new intro/pages.md`

Per default the pages are sorted by their name. If you wish another order, you can define the order with the parameter `weight`: 

```
---
title: "Pages"
date: 2022-04-26T12:42:03+02:00
draft: false
weight: 2
---
```

### Show page

URL: https://localhost:1313

#### GoHugo is installed

`hugo server`\
`hugo server -D`        # Unvisible pages inclusive

#### Docker is installed

`docker run -it --rm -v ./:/page --net host registry.gitlab.com/infmod/root:latest /bin/sh -c "cd /page; hugo server"`\
`docker run -it --rm -v ./:/page --net host registry.gitlab.com/infmod/root:latest /bin/sh -c "cd /page; hugo server -D"`        # Unvisible pages inclusive

